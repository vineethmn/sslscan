sslscan (2.0.12-0kali1) kali-dev; urgency=medium

  * Improve debian/rules
  * New upstream version 2.0.12
  * Add superficial autopkgtest
  * Bump Standards-Version to 4.6.0

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 24 Feb 2022 15:02:42 +0100

sslscan (2.0.11-0kali2) kali-dev; urgency=medium

  * Fix the build on armhf

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Dec 2021 11:32:13 +0100

sslscan (2.0.11-0kali1) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Improve build for CI
  * Fix openssl build for CI
  * Fix debian/patches/fix-ci-build.patch
  * New upstream version 2.0.11

  [ Ben Wilson ]
  * Update email address
  * Remove template comment and switch spaces to tabs

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 17 Dec 2021 10:29:11 +0100

sslscan (2.0.10-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.10
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 10 May 2021 14:10:48 +0200

sslscan (2.0.9-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.9

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 26 Mar 2021 08:57:17 +0100

sslscan (2.0.8-0kali1) kali-dev; urgency=medium

  * debian/kali-ci.yml: disable builds
  * New upstream version 2.0.8

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 22 Feb 2021 11:40:00 +0100

sslscan (2.0.7-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sophie Brun ]
  * New upstream version 2.0.7

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 11 Feb 2021 09:57:07 +0100

sslscan (2.0.6-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.6

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 03 Nov 2020 09:07:18 +0100

sslscan (2.0.5-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.5

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 27 Oct 2020 10:01:15 +0100

sslscan (2.0.4-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.4

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Oct 2020 14:12:09 +0200

sslscan (2.0.3-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.3

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 12 Oct 2020 15:35:12 +0200

sslscan (2.0.2-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.2

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 05 Oct 2020 11:16:46 +0200

sslscan (2.0.1-0kali2) kali-dev; urgency=medium

  * Add missing Vcs-Git headers

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 28 Sep 2020 13:51:17 +0200

sslscan (2.0.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 28 Sep 2020 13:51:15 +0200

sslscan (2.0.0-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 24 Jul 2020 11:54:07 +0200

sslscan (2.0.0~beta6-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0~beta6

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 03 Jul 2020 10:03:31 +0200

sslscan (2.0.0~beta4-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0~beta4

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 15 Jun 2020 10:07:42 +0200

sslscan (2.0.0~beta2-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0~beta2

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 11 May 2020 16:07:52 +0200

sslscan (2.0.0~beta1-0kali1) kali-dev; urgency=medium

  * Update debian/watch for beta versions
  * New upstream version 2.0.0~beta1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 23 Mar 2020 11:21:26 +0100

sslscan (2.0.0~alpha2-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0~alpha2
  * Reintroduce static build

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 05 Mar 2020 16:28:06 +0100

sslscan (2.0.0~alpha1-0kali1) kali-experimental; urgency=medium

  [ g0tmi1k ]
  * Fix git URL

  [ Raphaël Hertzog ]
  * Revert "Fix git URL"
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * Update debian/watch
  * New upstream version 2.0.0~alpha1
  * Remove useless patches
  * Fix debian/watch
  * Disable tests: it requires docker
  * Bump Standards-Version to 4.5.0
  * Add a patch to fix spelling error
  * Drop static build

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 24 Feb 2020 16:51:00 +0100

sslscan (1.11.13-rbsec-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 25 Mar 2019 16:08:37 +0100

sslscan (1.11.12-rbsec-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 19 Oct 2018 12:02:35 +0200

sslscan (1.11.11-rbsec-0kali2) kali-dev; urgency=medium

  * Add a patch to openssl to fix issue on armhf (see
    https://github.com/openssl/openssl/pull/4673)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 06 Feb 2018 14:45:36 +0100

sslscan (1.11.11-rbsec-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Keep static build (for SSLv2 support)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 30 Jan 2018 16:18:10 +0100

sslscan (1.11.10-rbsec-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Keep static build (for SSLv2 support)

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 05 May 2017 10:00:03 +0200

sslscan (1.11.9-rbsec-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 11 Apr 2017 16:04:17 +0200

sslscan (1.11.8-rbsec-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Keep static build (for SSLv2 support)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Nov 2016 14:30:39 +0100

sslscan (1.11.7-rbsec-kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Jun 2016 12:05:49 +0200

sslscan (1.11.6-rbsec-kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Keep static build (for SSLv2 support)
  * debian/rules: build without pie (because of static build)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Apr 2016 11:28:21 +0200

sslscan (1.11.5-rbsec-0kali1) unstable; urgency=medium

  * New Upstream release (Closes: #804616)
  * debian/control
    - Updated Standard-Version to 3.9.7.0
  * Updated debian/watch
  * Updated debian/rules to 3.0 (quilt) format
  * Updated debian/copyright
  * Upload to unstable
  * Removed useless patches
  * Removed git from Build-Depends

 -- Marvin Stark <marv@der-marv.de>  Fri, 01 Apr 2016 10:04:10 +0200

sslscan (1.11.5-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 25 Mar 2016 10:33:22 +0100

sslscan (1.11.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Mar 2016 11:32:44 +0100

sslscan (1.11.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 21 Dec 2015 14:50:46 +0100

sslscan (1.11.0-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 25 Sep 2015 15:07:19 +0200

sslscan (1.10.6-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 01 Sep 2015 15:20:34 +0200

sslscan (1.10.5~git2365584ac4-0kali1) experimental; urgency=medium

  * New upstream release (Closes: #765692)
    - Includes IPv6 support (Closes: #720391)
  * debian/control:
    - Updated Build-Dependencies for debhelper
    - Updated Standard-Version
    - Updated Homepage entry (Closes: #687662)
    - Removed Build-Dependency on quilt
    - Added git to Build-Dependencies
  * debian/compat:
    - Changed compat level to v9
  * debian/rules:
    - Removed patch and unpatch targets as they are not needed anymore
  * Updated debian/copyright to copyright-format 1.0
  * Switch to dpkg-source 3.0 (quilt) format
  * Removed README.source
  * Removed 03-sslv2.diff patch
  * Removed 02-sslscan-spelling-mistake.diff patch

 -- Marvin Stark <marv@der-marv.de>  Mon, 03 Aug 2015 13:01:54 +0000

sslscan (1.10.5-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Drop the patches: Fix-clean-test.patch, Fix-install-path.patch,
    Fix-flags.patch now included upstream

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 13 Jul 2015 18:04:40 +0200

sslscan (1.9.10-rbsec-0kali1) kali; urgency=medium

  [ Sophie Brun ]
  * Import new upstream release from rbsec git repository
  * Update debian/copyright, debian/watch, debian/compat
  * Add a debian/source/format file
  * Add a patch to fix LDFLAGS and CFLAGS
  * Add a patch to fix the installation paths
  * Add a patch to avoid failure when cleaning is made before building
  * Rewrite debian/rules and build statically against a version of
    OpenSSL that has SSLv2 support

  [ Raphaël Hertzog ]
  * Add support for CPPFLAGS too.

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 27 Jan 2015 15:15:02 +0100

sslscan (1.8.2-2) unstable; urgency=low

  * debian/control:
    - Updated Homepage (Closes: #633456)
    - Updated Standards-Version to 3.9.2
  * Fixed FTBFS with ld --as-needed (Closes: #639013)
  * Fixed FTBFS undefined reference to SSLv2_client_method (Closes: #622019)
  * debian/rules:
    - Added build-arch build-indep targets
    - Substituded dh_clean with dh_prep in install target

 -- Marvin Stark <marv@der-marv.de>  Thu, 22 Dec 2011 15:29:19 +0000

sslscan (1.8.2-0kali1) unstable; urgency=low

  * New Upstream release
  * debian/control:
    - updated Standards-Version to 3.8.3
  * Fixed FTBFS with binutils-gold (Closes: #556372)
  * Fixed spelling errors in sslscan.c
  * Added patch descriptions

 -- Marvin Stark <marv@der-marv.de>  Mon, 25 Jan 2010 19:28:33 +0100

sslscan (1.8.0-0kali1) unstable; urgency=low

  * New Upstream Version
    - Added SSL implementation workaround option
    - Added HTTP connection testing
    - Fixed Certification validation XML output
  * debian/control:
    - Updated Standards-Version to 3.8.1

 -- Marvin Stark <marv@der-marv.de>  Sun, 24 May 2009 11:47:24 +0200

sslscan (1.7.1-0kali1) unstable; urgency=low

  * New Upstream Version
  * Added debian/watch

 -- Marvin Stark <marv@der-marv.de>  Fri, 11 Jul 2008 10:51:30 +0200

sslscan (1.7-0kali1) unstable; urgency=low

  * New Upstream Version
  * debian/control:
    - Standards-Version changed to 3.8.0
  * debian/copyright:
    - Updated copyright years

 -- Marvin Stark <marv@der-marv.de>  Fri, 11 Jul 2008 10:32:01 +0200

sslscan (1.6-0kali1) unstable; urgency=low

  * New upstream release
    Improved certificate checking
    Added Makefile
    Added manpage
  * Patched Makefile
  * Added quilt as Build-Dependency

 -- Marvin Stark <marv@der-marv.de>  Tue, 22 Jan 2008 07:49:35 +0000

sslscan (1.5-0kali1) unstable; urgency=low

  * Initial release (Closes: #457217)

 -- Marvin Stark <marv@der-marv.de>  Thu, 20 Dec 2007 13:36:43 +0000
